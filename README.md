# pre-request
* postgres sql database
* python

# setup
1. execute _sql/create_schema.sql_ on your postgres database to create essential schemas
2. install python dependencies using `pip install -r requirements.txt`

# usage
1. initialize database:
`scrapy crawl all_title -s DB_HOST=<ip> -s DB_PORT=<port> -s DB_NAME=<database> -s DB_USER=<user> -s DB_PASS=<password>`
2. daily update new comics:
`scrapy crawl update_comic -s DB_HOST=<ip> -s DB_PORT=<port> -s DB_NAME=<database> -s DB_USER=<user> -s DB_PASS=<password>` 
