CREATE TABLE IF NOT EXISTS eight_comic_meta (
    id            integer PRIMARY KEY,
    title         text,
    title_aka     text,
    actor         text,
    kind          text,
    author        text,
    total         integer,
    is_finished   boolean,
    last_update   timestamp,
    vote_count    integer,
    total_score   integer,
    monthly_score integer,
    description   text,
    thumbnail     text
);

CREATE TABLE IF NOT EXISTS eight_comic_chapter (
    id            integer,
    title         text,
    comic_id      integer REFERENCES eight_comic_meta (id),
    PRIMARY KEY (comic_id, id)
);