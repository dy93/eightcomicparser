# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ComicMeta(scrapy.Item):
    # define the fields for your item here like:
    id = scrapy.Field()
    thumbnail = scrapy.Field()
    title = scrapy.Field()
    title_aka = scrapy.Field()
    actor = scrapy.Field()
    kind = scrapy.Field()
    author = scrapy.Field()
    total = scrapy.Field()
    status = scrapy.Field()
    last_update = scrapy.Field()
    vote_count = scrapy.Field()
    total_score = scrapy.Field()
    monthly_score = scrapy.Field()
    description = scrapy.Field()


class ComicChapter(scrapy.Item):
    comic_id = scrapy.Field()
    id = scrapy.Field()
    title = scrapy.Field()
