# -*- coding: utf-8 -*-
import scrapy
import logging
import re
from EightComicParser.items import ComicMeta, ComicChapter


class AllTitleSpider(scrapy.Spider):
    name = "all_title"
    allowed_domains = ["www.comicbus.com"]
    start_urls = ['http://www.comicbus.com/comic/all.html']
    MAX_PAGES = 10
    _pages = 0

    def parse(self, response):
        comic_hrefs = [a for a in response.xpath('//tr/td/a/@href') if a.extract().startswith('/html')]
        for comic_href in comic_hrefs:
            url = response.urljoin(comic_href.extract())
            self._pages += 1
            if self._pages < AllTitleSpider.MAX_PAGES:
                logging.warning('do {url}'.format(url=url))
                yield scrapy.Request(url, callback=self.parse_meta)
            else:
                logging.warning('max pages reached')
                break

    def parse_meta(self, response):
        meta = ComicMeta()

        match = re.search('/([0-9]+)\.html', response.url)
        meta['id'] = int(match.group(1))
        meta['thumbnail'] = response.urljoin(
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[1]/img/@src')[0].extract())
        meta['title'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table/tr[1]/td/table/tr[1]/td[2]/font/text()')[
                0].extract().strip()
        meta['title_aka'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table/tr[1]/td/table/tr[1]/td[2]/text()')[
                0].extract().strip()

        actor_strs = response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table/tr[3]/td/font/text()').extract()
        meta['actor'] = (actor_strs[0] if len(actor_strs) > 0 else '').strip()

        meta['kind'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[1]/td[2]/p/text()')[
                0].extract().strip()
        meta['author'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[3]/td[2]/text()')[
                0].extract().strip()
        meta['total'] = int(response.xpath(
            '/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[5]/td[2]/a/font/b/text()')[
                                0].extract().split('-')[-1])
        meta['status'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[5]/td[2]/a/text()')[
                0].extract().strip()
        meta['last_update'] = \
            response.xpath('/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[11]/td[2]/text()')[
                0].extract()
        score_str = response.xpath(
            '/html/body/table[2]/tr/td/table/tr[1]/td[2]/table[2]/tr/td[1]/table/tr[15]/td[2]/img/@alt')[0].extract()
        meta['vote_count'], meta['total_score'], meta['monthly_score'] = [int(s.strip().split(':')[-1]) for s in
                                                                          score_str.split(',')]
        meta['description'] = ''.join(
            response.xpath('/html/body/table[2]/tr/td/table/tr[3]/td/text()').extract()).strip()
        yield meta

        for a in [a for a in response.xpath('//a[@onclick]') if 'cview' in a.xpath('@onclick').extract()[0]]:
            chapter = ComicChapter()
            chapter['comic_id'] = meta['id']
            chapter['id'] = int(re.search('-([0-9]+).html', a.xpath('@onclick').extract()[0]).group(1))
            chapter['title'] = a.xpath('text()').extract()[0].strip()
            if chapter['title'] == '':
                chapter['title'] = a.xpath('font/text()').extract()[0].strip()
            yield chapter
