# -*- coding: utf-8 -*-
import scrapy
import logging
# from EightComicParser.items import ComicMeta, ComicChapter
from all_title import AllTitleSpider
from datetime import datetime, date, timedelta


class UpdateComicSpider(AllTitleSpider):
    name = 'update_comic'
    allowed_domains = ['v.comicbus.com']
    # 漫畫更新日誌
    _url_template = 'http://v.comicbus.com/comic/u-{page}.html'
    _page = 1
    start_urls = [_url_template.format(page=_page)]

    def parse(self, response):
        today_str = unicode(datetime.now().strftime('%Y年%m月%d日'), 'utf-8')
        yesterday_str = unicode((date.today() - timedelta(1)).strftime('%Y年%m月%d日'), 'utf-8')

        # filter updates of today and yesterday
        trs = [tr for tr in response.xpath('//tr[@onmouseover]') if
               tr.xpath('td[2]/text()')[0].extract() == today_str or
               tr.xpath('td[2]/text()')[0].extract() == yesterday_str]
        for tr in trs:
            tr.xpath('td[1]/a/@href')[0].extract()
            url = response.urljoin(tr.xpath('td[1]/a/@href')[0].extract())
            logging.warning('do {url}'.format(url=url))
            yield scrapy.Request(url, callback=self.parse_meta)

        # try to parse next page to get update comics
        if len(trs) > 0:
            self._page += 1
            logging.warning('do {url}'.format(url=self._url_template.format(page=self._page)))
            yield scrapy.Request(self._url_template.format(page=self._page))
