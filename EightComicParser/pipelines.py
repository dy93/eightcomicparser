# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import psycopg2
from datetime import datetime
from items import ComicMeta, ComicChapter


class EightComicBasicPipeline(object):
    db_host = "localhost"
    db_port = 5432
    db_name = "crawler"
    db_user = "crawl"
    db_pass = "password"
    conn = None

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings['DB_HOST'], int(settings['DB_PORT']), settings['DB_NAME'], settings['DB_USER'],
                   settings['DB_PASS'])

    def __init__(self, host, port, database, user, password):
        self.db_host, self.db_port, self.db_name = host, port, database
        self.db_user, self.db_pass = user, password

    def open_spider(self, spider):
        self.conn = psycopg2.connect(host=self.db_host, port=self.db_port, database=self.db_name, user=self.db_user,
                                     password=self.db_pass)

    def close_spider(self, spider):
        self.conn.close()


class EightComicParserPipeline(EightComicBasicPipeline):
    def process_item(self, item, spider):
        if not isinstance(item, ComicMeta):
            return item
        with self.conn:
            with self.conn.cursor() as cur:
                meta = dict(item)
                meta['is_finished'] = False if meta['status'] == u'連載中' else True
                meta['last_update'] = datetime.strptime(meta['last_update'], '%Y-%m-%d')
                meta['vote_count'] = int(meta['vote_count'])
                meta['total_score'] = int(meta['total_score'])
                meta['monthly_score'] = int(meta['monthly_score'])
                cur.execute("""
                INSERT INTO eight_comic_meta (
                    id,
                    title,
                    title_aka,
                    actor,
                    kind,
                    author,
                    total,
                    is_finished,
                    last_update,
                    vote_count,
                    total_score,
                    monthly_score,
                    description,
                    thumbnail)
                VALUES (
                    %(id)s,
                    %(title)s,
                    %(title_aka)s,
                    %(actor)s,
                    %(kind)s,
                    %(author)s,
                    %(total)s,
                    %(is_finished)s,
                    %(last_update)s,
                    %(vote_count)s,
                    %(total_score)s,
                    %(monthly_score)s,
                    %(description)s,
                    %(thumbnail)s)
                ON CONFLICT (id) DO UPDATE SET (
                    total, is_finished, last_update, vote_count, total_score, monthly_score) =
                    (%(total)s, %(is_finished)s, %(last_update)s, %(vote_count)s, %(total_score)s, %(monthly_score)s
                )
                """, meta)
        return item


class EightComicChapterPipeline(EightComicBasicPipeline):
    def process_item(self, item, spider):
        if not isinstance(item, ComicChapter):
            return item
        with self.conn:
            with self.conn.cursor() as cur:
                chapter = dict(item)
                cur.execute("""
                INSERT INTO eight_comic_chapter (
                    id,
                    comic_id,
                    title)
                VALUES (
                    %(id)s,
                    %(comic_id)s,
                    %(title)s)
                ON CONFLICT (comic_id, id) DO UPDATE SET
                    title = %(title)s
                """, chapter)
        return item
